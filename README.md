## Описание/Пошаговая инструкция выполнения домашнего задания:
1. Установить react-router;    
2. Добавить отдельные компоненты страниц - Login / Register / HomePage / 404;    
3. Добавить стейт-менеджемент с Redux;    
4. Найти возможное дублирование кода и применить HOC паттерн.    
