import React from 'react';
import {Navigate, createBrowserRouter} from "react-router-dom";

import App from '../App'
import ErrorPage from '../Pages/ErrorPage'
import HomePage from '../Pages/HomePage'
import {AboutPage,AboutCourse,AboutPeriod} from '../Pages/AboutPage'
import LoginPage from '../Pages/LoginPage'
import RegisterPage from '../Pages/RegisterPage'
import WelcomePage from '../Pages/WelcomePage'

import Paths from './Paths';


function CreateBrowserRouter(){
  const paths = Paths();

  return createBrowserRouter(
    [
      {
        path: paths.HOME,
        element: <App paths={paths}/>,
        errorElement: <ErrorPage />,
        children: 
        [
          {
            index:true,
            element: <HomePage/>
          },
          {
            path: paths.ABOUT,
            element: <AboutPage paths={paths}/>,
            children: 
            [
              {
                path: paths.ABOUT_COURSE,
                element: <AboutCourse />,
              },
              {
                path: paths.ABOUT_PERIOD,
                element: <AboutPeriod />,
              }
            ]
          },
          {
            path: paths.LOGIN,
            element: <LoginPage returnUrl={paths.WELCOME}/>,
          },
          {
            path: paths.REGISTER,
            element: <RegisterPage returnUrl={paths.WELCOME}/>
          },
          {
            path: paths.WELCOME,
            element: <WelcomePage navigationUnauthorized={<Navigate to={paths.LOGIN}/>}/>
          }
        ] 
      }
    ]
  )
}

const Router = CreateBrowserRouter();

export default Router;