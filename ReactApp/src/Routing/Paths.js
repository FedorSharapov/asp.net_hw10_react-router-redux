export default function Paths(){
    const mainPaths ={
        HOME: '/',
        ABOUT: '/about',
        ABOUT_COURSE:'/about/course',
        ABOUT_PERIOD:'/about/period',
        LOGIN: '/login',
        REGISTER: '/register',
        WELCOME: '/welcome'
    }

    Object.freeze(mainPaths);

    return mainPaths;    
}