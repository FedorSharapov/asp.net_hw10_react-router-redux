import './App.css';

import React from 'react';
import {Outlet } from "react-router-dom";

import NavBar from'./Components/NavBar/NavBar';
import Footer from './Components/Footer/Footer';


function App(props) {
  return (
    <div className="App">
      <NavBar paths={props.paths} titleHomePage="Counter"/>

      <main>
        <Outlet />
      </main>

      <Footer year={new Date().getFullYear()} text={'MyReactApp'}/>
    </div>
  );
}

App.defaultProps = {paths:{}};
export default App;