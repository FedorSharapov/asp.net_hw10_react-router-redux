import { createAction, createReducer } from "@reduxjs/toolkit";

/*
"Reducer" указывает, как изменяется "application's state" в ответ на "actions", отправленные в хранилище.
Помните, что "actions" описывают только то, что произошло, но не описывают, как изменяется "application's state".
*/

// application's state
const loginInitialState ={
    isAuthorized: false,
    userName: null
};

// actions 
export const loggedIn = createAction('LOGGEDIN');
export const loggedOut = createAction('LOGGEDOUT');
          
// Reducer
const LoginReducer = createReducer(loginInitialState, (builder) => {
    builder
        .addCase(loggedIn, (state, action) => {
            state.isAuthorized = true;
            state.userName = action.payload.userName;
        })
        .addCase(loggedOut, (state) =>{
            state.isAuthorized = false;
            state.userName = null;
        })
});

export default LoginReducer;