import storage from 'reduxjs-toolkit-persist/lib/storage'   // localStorage
import { combineReducers } from '@reduxjs/toolkit';
import {persistReducer  } from 'redux-persist'

import { configureStore } from '@reduxjs/toolkit'
import thunk from 'redux-thunk'
import persistStore from 'redux-persist/es/persistStore';

import LoginReducer from './LoginReducer';
import CounterReducer from './CounterReducer';


const reducers = combineReducers({
    login: LoginReducer,
    counter: CounterReducer
});

const persistConfig ={
    key: 'root',
    version: 1,
    storage
};

const persistedReducer = persistReducer(persistConfig,reducers);

export const store = configureStore({
    reducer: persistedReducer,
    devTools: process.env.NODE_ENV !== 'production',
    middleware: [thunk]
});

/* создание персистентности(возможность долговременного хранения состояния) для хранилища */
export const persistor = persistStore(store);