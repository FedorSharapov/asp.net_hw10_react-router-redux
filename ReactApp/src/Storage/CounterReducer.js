import { createSlice } from "@reduxjs/toolkit";

const counterSlice =createSlice({
    name:'counter',
    initialState:{
        value: 0
    },
    reducers:{
        decreased(state){
            state.value -= 1
        },
        increased(state){
            state.value += 1
        }
    }
})

export default counterSlice.reducer;
export const {increased, decreased} = counterSlice.actions;