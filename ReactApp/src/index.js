import './index.css';

import ReactDOM from 'react-dom/client'
import {Provider} from 'react-redux'
import {PersistGate} from 'redux-persist/integration/react';
import {store, persistor} from './Storage/Store';

import {RouterProvider} from "react-router-dom"
import Router from './Routing/BrowserRouter'


const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
    <Provider store={store}> 
        <PersistGate loading={<>...</>} persistor={persistor}>
            <RouterProvider router={Router} />
        </PersistGate>
    </Provider>
);