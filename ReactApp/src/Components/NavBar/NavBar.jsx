import './NavBar.css';

import React from "react";
import {NavLink} from "react-router-dom";

import {useSelector} from 'react-redux'


function NavBar(props) {
  const isAuthorized = useSelector(state => state.login.isAuthorized);
  const userName = useSelector(state => state.login.userName);

  return (
    <div className="navbar">
      <nav className="main">
        <NavLink to={props.paths.HOME}>{props.titleHomePage}</NavLink>
        <NavLink to={props.paths.ABOUT}>About</NavLink>
      </nav>
      <nav className="auth">
      {
        isAuthorized ?
        <div className='user'>
          <NavLink to={props.paths.LOGIN}>{userName}</NavLink>
        </div>
        :
        <div>
          <NavLink to={props.paths.LOGIN}>Login</NavLink>
          <NavLink to={props.paths.REGISTER}>Register</NavLink>
        </div>
      }
      </nav>
    </div>
  );
}

NavBar.defaultProps = {paths:{},titleHomePage:'Home'};
export default NavBar;