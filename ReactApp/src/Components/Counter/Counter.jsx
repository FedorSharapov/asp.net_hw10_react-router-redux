import './Counter.css';

function Counter(props){
    return <div >
        <br/>
        <div className="container">
            <h1>Counter</h1>
            <div className='counter'>
                <input className='button' type='button' value='-' onClick={props.decreaseClick}/>
                <div id="counter-value">{props.value}</div>
                <input className='button' type='button' value='+' onClick={props.increaseClick}/>
            </div>
        </div>
    </div>
}

Counter.defaultProps = {value:'0',decreaseClick :{},increaseClick:{}}
export default Counter;