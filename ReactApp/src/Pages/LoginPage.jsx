import {useDispatch, useSelector} from 'react-redux'
import {loggedIn,loggedOut} from '../Storage/LoginReducer'

import {useNavigate} from 'react-router-dom';

import LoginForm from '../Components/Forms/LoginForm/LoginForm';
import LogoutForm from '../Components/Forms/LogoutForm/LogoutForm';

import Login from '../Api/LoginApi';


function LoginPage(props){
    const isAuthorized = useSelector(state => state.login.isAuthorized);
    const userName = useSelector(state => state.login.userName);
    const dispatch = useDispatch();
    const navigate = useNavigate();

    async function loginHandler(){
        const userNameValue = document.getElementById('username1').value;
        const passwordValue = document.getElementById('password1').value;
        
        try{
            const result = await Login(userNameValue,passwordValue);
            if(result.status == 200)
            {
                dispatch(loggedIn({userName:result.body}));
                navigate(props.returnUrl);
            }
            else
                alert(`Error status: ${result.status}`) 
        }catch(err){
            alert(`Error: ${err.message}`)
        }
    }

    function logoutHandler(){
        dispatch(loggedOut());
    }

    return <div> 
        { 
            isAuthorized?
            <LogoutForm userName={userName} onLogoutClick={logoutHandler}/>:
            <LoginForm onLoginClick={loginHandler}/>
        }
    </div>

}

LoginPage.defaultProps = {returnUrl:'/'};
export default LoginPage;