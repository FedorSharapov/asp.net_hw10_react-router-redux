import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Grid from '@mui/material/Grid';
import { Alert } from '@mui/material';

import { useState } from 'react';
import {useDispatch} from 'react-redux'
import {loggedIn} from '../Storage/LoginReducer'
import {useNavigate} from 'react-router-dom';

import Register from '../Api/RegisterApi'


function RegisterPage(props){
    const [stateLogin, setStateLogin] = useState({isError:false,errMsg:''});

    const dispatch = useDispatch();
    const navigate = useNavigate();

    async function registerHandler(){
        const userNameValue = document.getElementById('reg_login1').value;
        const passwordValue = document.getElementById('reg_password1').value;
        const confirmpasswordValue = document.getElementById('reg_password2').value;

        try{
            const result = await Register(userNameValue,passwordValue,confirmpasswordValue);
            if(result.status == 200){
                dispatch(loggedIn({userName:result.body}));
                navigate(props.returnUrl);

                setStateLogin({isError:false, errMsg:' '});
            }
            else{
                setStateLogin({isError: true, errMsg: result.body});
            }
        } catch(err){
            alert(`Error: ${err.message}`)
        }
    }

    return <form className="register-form">
        <br/>
        <Grid
         container
         direction="column"
         justifyContent="flex-start"
         alignItems="center"
         spacing={3}
         border={'-moz-initial'}
        >
            <Grid item xs={6}>
                <TextField  id="reg_login1" label="Login" variant="outlined" size="small"/>
            </Grid>
            <Grid item xs={6}>
                <TextField id="reg_password1" label="Password" variant="outlined" size="small"/>
            </Grid>
            <Grid item xs={6}>
                <TextField id="reg_password2" label="Confirm password" variant="outlined" size="small"/>
            </Grid>
            <Grid item xs={6}>
                <Button variant="outlined" onClick={registerHandler}>Register</Button>
            </Grid>
            <Grid item xs={6}>
            {
                stateLogin.isError ?
                <Alert severity="error">{stateLogin.errMsg}</Alert>:
                <></>
            }
            </Grid>
        </Grid>
    </form>
}

RegisterPage.defaultProps = {returnUrl:'/'};
export default RegisterPage;