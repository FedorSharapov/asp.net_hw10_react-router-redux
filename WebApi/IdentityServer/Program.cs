var builder = WebApplication.CreateBuilder(args);

const string _myAllowSpecificOrigins = "ReactApp";

builder.Services.AddCors(options =>
{
    options.AddPolicy(name: _myAllowSpecificOrigins, 
        policy =>
        {
            // ��� ����� ������� ������� ����� POST � ��������� Content-Type
            policy.WithOrigins("http://localhost:3000")
            .WithMethods("POST") 
            .WithHeaders("Content-Type");
        });
    options.AddPolicy(name: "Localhost:4000_AllowAnyMethod_AllowAnyHeader",
        policy =>
        {
            // ��� ����� ������� �������� ����� ������ � ���������
            policy.WithOrigins("http://localhost:4000")
            .AllowAnyMethod()
            .AllowAnyHeader();
        });
});
builder.Services.AddControllers();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

// app.UseCors(_myAllowSpecificOrigins);    // � ����� ������� �������� _myAllowSpecificOrigins ����� ��������� �� ���� ������������ � �� ������� ���������
app.UseCors();      
app.MapControllers();

app.Run();
